package com.example.try7

import dagger.Module
import dagger.Provides
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class CreateRetrofit @AssistedInject constructor(
    @Assisted private val url: String
) {
    private var level = "BODY"

    @AssistedFactory
    interface Factory {
        fun create(url: String): CreateRetrofit
    }

    fun setLevel(_level: String) {
        this.level = _level
    }

    @Provides
    fun provideOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(
                HttpLoggingInterceptor().setLevel(
                    HttpLoggingInterceptor.Level.valueOf(
                        level.uppercase()
                    )
                )
            )
            .build()

    @Provides
    fun provideBuildRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl(url)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()
    }

    @Provides
    fun provideCreateRetrofitBelarusBank(): TestClevertecAPI {
        return provideBuildRetrofit(provideOkHttpClient())
            .create(TestClevertecAPI::class.java)
    }
}