package com.example.try7.elements

import com.google.gson.annotations.SerializedName

data class Meta(
    @SerializedName("title") val title: String,
    @SerializedName("image") val image: String,
    @SerializedName("fields") val fields: List<Field>
)
