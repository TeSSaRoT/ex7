package com.example.try7.elements

import com.google.gson.annotations.SerializedName


data class Field(
    @SerializedName("title") val title: String,
    @SerializedName("name") val name: String,
    @SerializedName("type") val type: String,
    @SerializedName("values") val values: HashMap<String, String>
)