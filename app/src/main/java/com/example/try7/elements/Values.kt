package com.example.try7.elements

import com.google.gson.annotations.SerializedName

data class Values(
    @SerializedName("form") val form: Map<String, String>
)
