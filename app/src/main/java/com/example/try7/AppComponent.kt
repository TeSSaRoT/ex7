package com.example.try7

import dagger.Component

@Component(modules = [CreateRetrofit::class])
interface AppComponent {
    val createRetrofitFactory: CreateRetrofit.Factory
}