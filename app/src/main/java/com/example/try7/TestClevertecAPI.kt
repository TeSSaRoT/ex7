package com.example.try7

import com.example.try7.elements.Meta
import com.example.try7.elements.Values
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface TestClevertecAPI {

    @GET("meta/")
    fun getMeta(): Single<Meta>

    @POST("data/")
    fun postForm(@Body value: Values): Single<HashMap<String, String>>
}