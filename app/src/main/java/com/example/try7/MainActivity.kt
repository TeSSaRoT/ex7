package com.example.try7

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.try7.adapters.EnterTextAdapter
import com.example.try7.databinding.ActivityMainBinding
import com.example.try7.elements.Field
import java.util.*


class MainActivity : AppCompatActivity() {

    private val model: MainViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding
    private lateinit var enterTextAdapter: EnterTextAdapter
    private val fieldList = ArrayList<Field>()
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val titleObserver = Observer<String> {
            supportActionBar?.title = it
        }

        val imageObserver = Observer<String> {
            Glide.with(this).load(it).into(findViewById(R.id.imageView))
        }

        val fieldObserver = Observer<Field> {
            enterTextAdapter = EnterTextAdapter(fieldList)
            init(it)
        }

        val responseObserver = Observer<HashMap<String, String>> {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(it.keys.toString())
                .setMessage(it.values.toString())
                .setPositiveButton("Close") { dialog, _ ->
                    dialog.cancel()
                }
            builder.create()
            builder.show()
        }

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        model.response.observe(this, responseObserver)
        model.title.observe(this, titleObserver)
        model.image.observe(this, imageObserver)
        model.field.observe(this, fieldObserver)

        getForm()
    }

    override fun onDestroy() {
        model.clearDisposable()
        super.onDestroy()
    }

    private fun getForm() {
        progressBar = findViewById(R.id.progressBar)
        Thread {
            this@MainActivity.runOnUiThread {
                progressBar.visibility = View.VISIBLE
            }

            Thread.sleep(1000) // Чтобы было видно что работает
            model.getMeta()

            this@MainActivity.runOnUiThread {
                progressBar.visibility = View.GONE
            }
        }.start()
    }

    fun send(view: View) {
        Thread {
            this@MainActivity.runOnUiThread {
                progressBar.visibility = View.VISIBLE
            }
            Thread.sleep(1000) // Чтобы было видно что работает
            model.postForm(enterTextAdapter.dataKV)

            this@MainActivity.runOnUiThread {
                progressBar.visibility = View.GONE
            }
        }.start()
    }

    private fun init(field: Field) {
        binding.recyclerView.layoutManager = LinearLayoutManager(applicationContext)
        binding.recyclerView.adapter = enterTextAdapter
        enterTextAdapter.addField(field)
    }
}