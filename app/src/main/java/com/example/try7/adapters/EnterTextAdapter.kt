package com.example.try7.adapters

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import com.example.try7.R
import com.example.try7.databinding.EnterNumberBinding
import com.example.try7.databinding.EnterTextBinding
import com.example.try7.databinding.SelectRadioBinding
import com.example.try7.elements.Field

class EnterTextAdapter(private val fieldList: ArrayList<Field>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var dataKV: HashMap<String, String> = HashMap()

    override fun getItemViewType(position: Int): Int {
        return when (fieldList[position].type) {
            "TEXT" -> 1
            "NUMERIC" -> 2
            "LIST" -> 3
            else -> 0
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            1 -> {
                val view =
                    LayoutInflater.from(parent.context).inflate(R.layout.enter_text, parent, false)
                return EnterTextHolder(view)
            }
            2 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.enter_number, parent, false)
                return EnterNumberHolder(view)
            }
            3 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.select_radio, parent, false)
                return SelectRadioHolder(view)
            }
            else -> {
                val view =
                    LayoutInflater.from(parent.context).inflate(R.layout.enter_text, parent, false)
                return EnterTextHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val field = fieldList[position]
        when (holder) {
            is EnterTextHolder -> {
                holder.binding.title1.title.text = field.title

                holder.binding.editTextTextPersonName.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        dataKV[field.name] = s.toString()
                    }

                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {

                    }
                })
            }
            is EnterNumberHolder -> {
                holder.binding.title2.title.text = field.title

                holder.binding.editTextNumberDecimal.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        if (s != null) {
                            dataKV[field.name] =
                                if (s.isEmpty()) {
                                    s.toString()
                                } else fmt(s.toString().toDouble())
                        }
                    }

                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {

                    }
                })
            }
            is SelectRadioHolder -> {
                holder.binding.title3.title.text = field.title
                field.values.keys.forEach {
                    val newRadioButton = RadioButton(holder.binding.radioGroup.context)
                    newRadioButton.text = field.values[it].toString()
                    newRadioButton.tag = it
                    holder.binding.radioGroup.addView(newRadioButton)
                }

                holder.binding.radioGroup.setOnCheckedChangeListener { radioGroup, i ->
                    dataKV[field.name] = radioGroup[i - 1].tag.toString()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return fieldList.size
    }

    fun addField(field: Field) {
        fieldList.add(field)
        notifyItemChanged(fieldList.size)
    }

    fun fmt(d: Double): String {
        return if (d == d.toLong().toDouble()) String.format(
            "%d",
            d.toLong()
        ) else String.format("%s", d)
    }
}

class EnterTextHolder(item: View) : RecyclerView.ViewHolder(item) {
    val binding = EnterTextBinding.bind(item)
}

class EnterNumberHolder(item: View) : RecyclerView.ViewHolder(item) {
    val binding = EnterNumberBinding.bind(item)
}

class SelectRadioHolder(item: View) : RecyclerView.ViewHolder(item) {
    val binding = SelectRadioBinding.bind(item)
}