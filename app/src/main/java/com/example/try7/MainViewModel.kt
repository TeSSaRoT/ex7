package com.example.try7

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.try7.elements.Field
import com.example.try7.elements.Values
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

const val baseUrl = "http://test.clevertec.ru/tt/"
const val LevelBODY = "BODY"

class MainViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private lateinit var createRetrofit: CreateRetrofit

    val title: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    val field: MutableLiveData<Field> by lazy {
        MutableLiveData<Field>()
    }

    val image: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    val response: MutableLiveData<HashMap<String, String>> by lazy {
        MutableLiveData<HashMap<String, String>>()
    }

    @Inject
    fun getMeta() {
        val app: AppComponent = DaggerAppComponent.create()
        createRetrofit = app.createRetrofitFactory.create(
            url = baseUrl
        )
        createRetrofit.setLevel(LevelBODY)
        val retrofit = createRetrofit.provideCreateRetrofitBelarusBank()
        compositeDisposable.add(
            retrofit.getMeta()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    title.value = it.title
                    image.value = it.image
                    it.fields.forEach { fields ->
                        field.value = fields
                    }
                },
                    {
                        print(it)
                    })
        )
    }

    fun postForm(gson: Map<String, String>) {
        val retrofit = CreateRetrofit(baseUrl).provideCreateRetrofitBelarusBank()
        compositeDisposable.add(
            retrofit.postForm(Values(gson))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    response.value = it
                },
                    {
                        print(it)
                    })
        )
    }

    fun clearDisposable() {
        compositeDisposable.dispose()
    }
}